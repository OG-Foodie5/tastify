On 2/15/23, the team worked on creating a YAML file for our project and adjusted the settings and permissions for our repository.

On 2/21/23, - the team focused on developing endpoints using FastAPI. However, we faced a blocker while attempting to create a migration. We took the opportunity to refine our member responsibilities during this period.

2/22/23 - We switched to PostgreSQL for our database and I took on the task of styling the login page to match the overall design of the website. This decision came after experiencing difficulties with SQL Alchemy.

2/24/23 - The team has completed the task of running endpoints and performing migrations. They have successfully deployed the endpoints for "get/create recipe", "create user", and "get user".

2/27/23 - The team was engaged in developing individual endpoints, while I focused on creating the "Login and get all users" endpoint. Additionally, I was in the planning phase of developing the front-end Login-up form and JWT authetication

3/1/23 - I completed the creation of a JWT script to authenticate and validate the login endpoint in FastAPI. The process involved generating a secret key, encoding and decoding the JWT token, and implementing authentication middleware, followed by thorough testing to ensure its proper functionality

3/6/23 - completed the creation of a JWT script to authenticate and validate the login endpoint on the front end of our application, using the React framework. Followed by thorough testing to ensure its proper functionality

3/8/23 - Fine tuned the login Form and started testing out endpoints, to make sure functionality of formed worked

3/9/23 - I contributed to improving the syntax and documentation of our pytest code. Though we encountered import-related issues in the test file, we were able to overcome them and ensure seamless functionality.

3/10/23 - I made further refinements to the login form and began testing various endpoints to ensure the form was functioning as intended. 
