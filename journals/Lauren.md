# 2/14/23
Today we finalized our MVP and a rough visualization of our wireframe. We forked the repo and adjusted its settings/permissions.

# 2/15/23
We all took a look at and worked on the docker yaml file, then proceeded to create individual branches. We set out to work on databases, refining the team member breakdown, and discuss issue/merge requests/templates on Gitlab.

# 2/16/23
Today we mostly worked on troubleshooting as a group. We got the dockerfile and individual branches working, and then we each downloaded Beekeeper and screen-shared to practice merge request procedures.

# 2/21/23
After setting up SQLAlchemy towards the end of last week, we attempted to do one endpoint as a group, and FastAPI. We refined the responsibility distribution a little more, and as a group we tried to overcome our blocker, which was trying to make a migration.

# 2/22/23
We did FastAPI, an endpoint, and finalized member point distribution/responsibilities. All our us individually ran through Paul's alembic tutorial and SQLAlchemy tutorials, both of which we ended up scrapping due to time concerns and constraints. 

# 2/24/23
Today the group managed to make migrations; get/create recipe, create user, get user endpoints are up and running.

# 2/27/23
We did our individual endpoints, I took on create-account and planned to do front-end create account/sign up form towards the end of the day but had to deal with more troubleshooting before tackling the front-end.

# 2/28/23
We continued working on our assigned front-end pages, routes, and paths. I continued testing the username and email field on my sign up page as well as the CSS which took a while to get right (after a few minor line changes and typos.)  I ended up scraping my sign-up.css file, and Jose suggested I use his CSS formatting in his login front-end in my sign up front-end for the sake of consistency/aesthetics/matching everything up.

# 3/1/23
After a couple back-end mishaps/blockers and some tweaking to set the back-end straight, I tackled the navigation bar, first making sure it had login and sign up functionality. 

# 3/6/23
The sign up form kept throwing a 400 and a 401 unauthorized error, which, after a few hours, was fixed by changing a line/path in routers.

# 3/7/23
With the deadline coming up, the team discussed priorities with deployment and pytesting. Rosheen and Paul stopped by our breakout room and guided us through deployment.

# 3/8/23
I fixed the navigation bar so that when a user is logged out, they view the sign up page and login/sign up links on the navigation bar. I made sure that when a user signs up, they are redirected to the login, and when they are logged in they should be able to see the landing page and other signed in user functionalities without the sign/up login links still populating the navigation bar. 

# 3/9/23
While Rosheen and Paul help with part 3 of deployment in the breakout room, I focused on pytest syntax/documentation, and the group encountered a few blockers with imports in the test file. After the fix we got all of our tests to pass and ran flake8/black, made our merge requests; with me accidentally just working on main but running flake8 and making sure things were still running smoothly for all ends. I got create a recipe and get all recipes tests to work, and started the README.md, docs/images folders, API.md, and Frontend.md.

# 3/10/23
Today we are fixing a few bugs in the urls and routes, going down the rubric to make sure we've pinned everything, and grouping up to polish the README.MD.