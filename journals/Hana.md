February 15:
Got started debugging some docker issues with the ghi container not opening. Tracy helped us with everything and it turns out we were just missing one line from our docker-compose.yaml oops…

February 16:
Today we kept working on getting sqlalchemy going. We installed a few things from it but we were mostly all working on Jose’s screen and doing a big group programming session.

February 17:
Looked even more at sqlalchemy stuff, but not a lot of progress on that front. Started looking at the database migration stuff to hopefully get going on that.

February 21:
STILL trying to get alembic working but we’re trying to work on getting some endpoints done in the meantime. Still haven’t finished that yet but we’re chugging along. Got beekeeper working I think (after switching to the free version) so now we can browse a few things in there

February 22:
Scrapped sqlalchemy since it’s taking waaaaay too long to set up and whatever time it would save us we ended up burning through during the setup anyway. We went through the entire tutorial that Paul posted and some other youtube videos but still couldn’t get it running so it’ll just have to live in the recycle bin for now sorry :(

February 23:
Watched most of the fastAPI videos from Curtis today and went through some of the documentation stuff but still kinda scared of touching too many things. I think some things were messed up with our pg admin site?? We can’t add a server for some reason but I’m not sure if that matters too much.

February 24:
Fixed up the migrations file so our tables are made! Had to double check a lot of SQL docs for the varchar and int stuff but it looks like things are working!

February 27:
Finished getting some more endpoints set up for the backend and now we should be able to delete a recipe.

February 28:
Did some more testing with the delete and now we can also update the recipe and get the details of a single recipe. Mostly followed along with the videos on learn, but I had to look up a few things for extra reference.

March 1:
Finished up the likes table in the migrations (and it works on the backend) so now it’ll record which recipe and user is related to the likes and then store that as an individual like in the database. Not sure if this is the most efficient way to do it but I can’t find much documentation for likes specifically online :(( We’ll embrace the tech debt later I guess?? We also got together as a group and set up all the routes for our App.js so now we can start moving to working on the frontend.

March 2:
Spent most of the day trying to fix the dumb “nOt FoUnD iN rEaCt RoUtEr DoM” error that was showing up on all of our pages. Once again the fix was adding exactly one line to most of the files that we had woohooo……….Tracy helped us so much with fixing the issue I’m honestly sad we didn’t ask for her help earlier because the fix ended up being really simple.

March 3:
Started working on the detail page. Took a long time but I finally managed to get recipe information to show up on the page yaaayy. Had to look back at the old react projects from mod 2, but everything finally got mapped properly. I made everything all cute and pink just for funsies while I work on debugging stuff. Also added a silly picture of a clown where the recipe goes which is definitely helping. Still can’t figure out why everything shows up on one page though???

March 4:
Started working on the comments box today. I’m able to grab the user_id from the logged in user but I had to put a very silly workaround that lets you select which recipe specifically you want to add the comment to. I think fixing the issue with showing a single recipe on the page might fix it but I’m not sure.

March 5:
I’m in like button purgatory. I haven’t seen my family in weeks. The like button has a mind of its own and is holding me hostage. For some reason the counter goes from 3 likes to 6 likes to 31 likes to 2 likes every time I click it????? But on the actual component page it shows the correct number I have no idea why. Stayed up too late working on it so I’m bouncing between this and fixing the comments box.

March 6:
Ok NOW the details page filters by recipe (I love you so much useParams my dear sweet angel)!! The comments are still messed up but we’re making progress at least. Definitely should have just checked in with Jake during this because useParams ended up being the fix for both of our pages so whoopsie.

March 7:
Now the comments are properly filtered too!!!!! AND managed to create them from the frontend yaaaaaay!! Still need to refresh the page to get it to show up but I’ll take what I can get.

March 8:
Worked on getting the pipeline stuff working today (had to add a credit card so I sure hope gitlab never has a big data breach or anything haha). I didn’t realize you could click on the job and see where the actual error and where it failed but now I do so ya learn something every day. Going through everything with deployment right now so now part 1 and 2 are finished. Very very nervous and stressed about the rest of deployment because the deadline is very very very soon.

March 9:
Deployment purgatory all day BUT all the instructors helped us get it working!!!!!!!!!! Stupid trailing slashes ruining everyone’s day but luckily everything was deployed properly on the backend!! Also I accidentally clogged up all the merge requests (there are like fifty ‘Deployment fixes’ on there from having to test it so many times, but we’ll just pretend they aren’t there). Double also I’m pretty sure I used up most of our minutes today but it’s not like we’re saving them for a special occasion or something. Triple also I really quickly whipped up my unit test and it’s working smoothly which is great

March 10:
Fixed up urls and now the site is all good!

