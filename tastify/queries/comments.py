from pydantic import BaseModel
from typing import List, Union, Optional
from queries.pool import pool


class Error(BaseModel):
    message: str


class CommentIn(BaseModel):
    content: str
    recipe_id: int
    user_id: int


class CommentOut(BaseModel):
    id: int
    content: str
    recipe_id: int
    user_id: int


class CommentRepository:
    def create(self, comment: CommentIn) -> CommentOut:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our INSERT statement
                    result = db.execute(
                        """
                        INSERT INTO comments
                            (content, recipe_id, user_id)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id;
                        """,
                        [comment.content, comment.recipe_id, comment.user_id],
                    )
                    id = result.fetchone()[0]
                    old_data = comment.dict()
                    return CommentOut(id=id, **old_data)
        except Exception:
            return {"message": "Create did not work"}

    def delete(self, comment_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM comments
                        WHERE id = %s
                        """,
                        [comment_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_all(self) -> Union[Error, List[CommentOut]]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        # Run our SELECT statement
                        """
                        SELECT id, content, user_id, recipe_id
                        FROM comments
                        """
                    )
                    result = db.fetchall()
                    return [
                        CommentOut(
                            id=id,
                            content=content,
                            user_id=user_id,
                            recipe_id=recipe_id,
                        )
                        for id, content, user_id, recipe_id in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all comments"}

    def get_one_comment(self, comment_id: int) -> Optional[CommentOut]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our SELECT statement
                    result = db.execute(
                        """
                        SELECT id
                                , content
                                , recipe_id
                                , user_id
                        FROM comments
                        WHERE id = %s
                        """,
                        [comment_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_comment_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve comment"}

    def record_to_comment_out(self, record):
        return CommentOut(
            id=record[0],
            content=record[1],
            recipe_id=record[2],
            user_id=record[3],
        )
