from pydantic import BaseModel
from typing import List, Optional, Union
from queries.pool import pool


class RecipeIn(BaseModel):
    title: str
    description: str
    steps: str
    ingredients: str
    picture_url: str
    user_id: int


class RecipeOut(BaseModel):
    id: int
    title: str
    description: str
    steps: str
    ingredients: str
    picture_url: str
    user_id: int


class RecipeOutList(BaseModel):
    recipes: list[RecipeOut]


class Error(BaseModel):
    message: str


class LikeIn(BaseModel):
    recipe_id: int
    user_id: int


class LikeOut(BaseModel):
    id: int
    recipe_id: int
    user_id: int


class RecipeRepository:
    def create(self, recipe: RecipeIn) -> RecipeOut:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our INSERT statement
                    result = db.execute(
                        """
                        INSERT INTO recipes
                            (title, description, steps, ingredients, picture_url, user_id)
                        VALUES
                            (%s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            recipe.title,
                            recipe.description,
                            recipe.steps,
                            recipe.ingredients,
                            recipe.picture_url,
                            recipe.user_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = recipe.dict()
                    return RecipeOut(id=id, **old_data)
        except Exception:
            return {"message": "Create did not work"}

    def recipe_in_to_out(self, id: int, recipe: RecipeIn) -> RecipeOut:
        old_data = recipe.dict()
        return RecipeOut(id=id, **old_data)

    def recipe_record_to_dict(self, row, description):
        recipe = None
        if row is not None:
            recipe = {}
            recipe_fields = [
                "recipe_id",
                "title",
                "description",
                "steps",
                "ingredients",
                "picture_url",
            ]
            for i, column in enumerate(description):
                if column.name in recipe_fields:
                    recipe[column.name] = row[i]
            recipe["id"] = recipe["recipe_id"]
            del recipe["recipe_id"]

            user = {}
            user_fields = [
                "user_id",
                "username",
            ]
            for i, column in enumerate(description):
                if column.name in user_fields:
                    user[column.name] = row[i]
            user["id"] = user["user_id"]
            del user["user_id"]
            recipe["user"] = user
        return recipe

    def get_all_recipes(self) -> Union[dict, List[RecipeOut]]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        users.id as user_id
                        , users.username
                        , recipes.id as recipe_id
                        , recipes.title
                        , recipes.description
                        , recipes.ingredients, steps
                        , recipes.picture_url
                        FROM users
                        JOIN recipes ON(users.id = recipes.user_id)
                        ORDER BY users.id;
                        """
                    )
                    result = []
                    rows = db.fetchall()
                    for row in rows:
                        recipe = self.recipe_record_to_dict(
                            row, db.description
                        )
                        result.append(recipe)
                    return result

        except Exception as e:
            print(e)
            return {"message": "Could not get all recipes"}

    def get_one(self, recipe_id: int) -> Optional[RecipeOut]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our SELECT statement
                    result = db.execute(
                        """
                        SELECT id
                                , title
                                , description
                                , steps
                                , ingredients
                                , picture_url
                                , user_id
                        FROM recipes
                        WHERE id = %s
                        """,
                        [recipe_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_recipe_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that recipe"}

    def delete(self, recipe_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM recipes
                        WHERE id = %s
                        """,
                        [recipe_id],
                    )
                    return True
        except Exception:
            return False

    def update(
        self, recipe_id: int, recipe: RecipeIn
    ) -> Union[RecipeOut, Error]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run sql with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE recipes
                        SET title = %s
                            , description = %s
                            , steps = %s
                            , ingredients = %s
                            , picture_url = %s
                            , user_id = %s
                        WHERE id = %s
                        """,
                        [
                            recipe.title,
                            recipe.description,
                            recipe.steps,
                            recipe.ingredients,
                            recipe.picture_url,
                            recipe.user_id,
                            recipe_id,
                        ],
                    )
                    return self.recipe_in_to_out(recipe_id, recipe)

        except Exception as e:
            print(e)
            return {"message": "Could not update recipe"}

    def record_to_recipe_out(self, record):
        return RecipeOut(
            id=record[0],
            title=record[1],
            description=record[2],
            steps=record[3],
            ingredients=record[4],
            picture_url=record[5],
            user_id=record[6],
        )


class LikeRepository:
    def add_like(self, like: LikeIn) -> LikeOut:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our INSERT statement
                    result = db.execute(
                        """
                        INSERT INTO likes
                            (recipe_id, user_id)
                        VALUES
                            (%s, %s)
                        RETURNING id;
                        """,
                        [like.recipe_id, like.user_id],
                    )
                    id = result.fetchone()[0]
                    return self.like_in_to_out(id, like)
        except Exception:
            return {"message": "Could not add like"}

    def delete_like(self, like_id: int) -> bool:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM likes
                        WHERE id = %s
                        """,
                        [like_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def like_in_to_out(self, id: int, like: LikeIn) -> LikeOut:
        old_data = like.dict()
        return LikeOut(id=id, **old_data)

    def get_all_likes(self) -> Union[dict, List[LikeOut]]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, user_id, recipe_id
                        FROM likes
                        ORDER BY id;
                        """
                    )
                    result = []
                    for record in db:
                        like = LikeOut(
                            id=record[0],
                            user_id=record[1],
                            recipe_id=record[2],
                        )
                        result.append(like)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not display likes"}

    def get_one_like(self, like_id: int) -> Optional[LikeOut]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our SELECT statement
                    result = db.execute(
                        """
                        SELECT id
                                , recipe_id
                                , user_id
                        FROM likes
                        WHERE id = %s
                        """,
                        [like_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return LikeOut(
                        id=record[0], recipe_id=record[1], user_id=record[2]
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not get that like"}
