import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.users import (
    UserRepository,
    UserOut,
    UserOutWithPassword,
)


class UserAuthenticator(Authenticator):
    async def get_account_data(self, username: str, accounts: UserRepository):
        return accounts.get(username)

    def get_account_getter(self, accounts: UserRepository = Depends()):
        return accounts

    def get_hashed_password(self, account: UserOutWithPassword):
        return account.hashed_password

    def get_account_data_for_cookie(self, account: UserOut):
        return account.username, UserOut(**account.dict())

    # async def login(self, response: Response, request: Request, form: AccountForm, accounts: UserRepository) -> dict:
    #     print("Inside login method!")
    #     ...
    #     print("Token: ", Token)
    #     return Token


authenticator = UserAuthenticator(os.environ["SIGNING_KEY"])
