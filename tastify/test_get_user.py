from fastapi.testclient import TestClient
from main import app
from queries.users import UserRepository

client = TestClient(app)


mock_user = {
    "id": 1,
    "username": "demo",
    "hashed_password": "demo",
    "email": "demo@demo.com"
}


class MockUserQueries:
    def get(self, username):
        return [mock_user] if username == mock_user["username"] else None


def test_get_user_id():
    app.dependency_overrides[UserRepository] = MockUserQueries
    response = client.get("/get/demo")
    assert response.status_code == 200
    app.dependency_overrides = {}
