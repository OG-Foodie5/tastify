from fastapi import APIRouter, Depends, Response
from queries.comments import (
    CommentIn,
    CommentRepository,
    CommentOut,
    Error,
)
from typing import Union


router = APIRouter()


@router.get("/comments/all", tags=["Comments"])
def get_all_comments(
    repo: CommentRepository = Depends(),
):
    comments = repo.get_all()
    return {"comments": comments}


@router.delete(
    "/comments/{comment_id}", response_model=bool, tags=["Comments"]
)
def delete_comment(
    comment_id: int,
    repo: CommentRepository = Depends(),
) -> bool:
    return repo.delete(comment_id)


@router.post("/create", tags=["comments"])
def create_comment(comment: CommentIn, repo: CommentRepository = Depends()):
    return repo.create(comment)


@router.get(
    "/comments/{comment_id}",
    tags=["Comments"],
    response_model=Union[CommentOut, Error],
)
def get_a_comment(
    comment_id: int,
    response: Response,
    repo: CommentRepository = Depends(),
) -> CommentOut:
    comment = repo.get_one_comment(comment_id)
    if comment is None:
        response.status_code = 404
    return comment
