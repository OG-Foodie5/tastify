from fastapi import APIRouter, Response, Depends
from queries.recipes import (
    RecipeIn,
    RecipeOut,
    RecipeRepository,
    Error,
    LikeIn,
    LikeOut,
    LikeRepository,
)
from typing import Union

router = APIRouter()


@router.get("/recipes", tags=["recipes"])
def get_all(
    repo: RecipeRepository = Depends(),  # RecipeRepository object to interact with the database
):
    recipes = repo.get_all_recipes()
    return {"recipes": recipes}


@router.post("/recipes", tags=["recipes"])
def create_recipe(recipe: RecipeIn, repo: RecipeRepository = Depends()):
    return repo.create(recipe)


@router.put(
    "/recipes/{recipe_id}",
    tags=["recipes"],
    response_model=Union[RecipeOut, Error],
)
def update_recipe(
    recipe_id: int,
    recipe: RecipeIn,
    repo: RecipeRepository = Depends(),
) -> Union[Error, RecipeOut]:
    return repo.update(recipe_id, recipe)


@router.get(
    "/recipes/{recipe_id}",
    tags=["recipes"],
    response_model=Union[RecipeOut, Error],
)
def get_one_recipe(
    recipe_id: int,
    response: Response,
    repo: RecipeRepository = Depends(),
) -> RecipeOut:
    recipe = repo.get_one(recipe_id)
    if recipe is None:
        response.status_code = 404
    return recipe


@router.delete("/recipes/{recipe_id}", tags=["recipes"], response_model=bool)
def delete_recipe(
    recipe_id: int,
    repo: RecipeRepository = Depends(),
) -> bool:
    return repo.delete(recipe_id)


@router.post("/like", tags=["Likes"], response_model=Union[LikeOut, Error])
def create_like(like: LikeIn, repo: LikeRepository = Depends()):
    return repo.add_like(like)


@router.delete("/delete/like", tags=["Likes"], response_model=bool)
def remove_like(
    like_id: int,
    repo: LikeRepository = Depends(),
) -> bool:
    return repo.delete_like(like_id)


@router.get("/likes", tags=["Likes"])
def get_likes(
    repo: LikeRepository = Depends(),  # RecipeRepository object to interact with the database
):
    likes = repo.get_all_likes()
    return {"likes": likes}


@router.get(
    "/likes/{recipe_id}", tags=["Likes"], response_model=Union[LikeOut, Error]
)
def get_a_like(
    recipe_id: int,
    response: Response,
    repo: LikeRepository = Depends(),
) -> LikeOut:
    recipe = repo.get_one_like(recipe_id)
    if recipe is None:
        response.status_code = 404
    return recipe
