steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(255) NOT NULL,
            hashed_password VARCHAR(250) NOT NULL,
            email VARCHAR(100) NOT NULL UNIQUE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """,
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE recipes (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(250) NOT NULL,
            description TEXT NOT NULL,
            steps TEXT NOT NULL,
            ingredients TEXT NOT NULL,
            picture_url TEXT NOT NULL,
            user_id INTEGER NOT NULL REFERENCES users(id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE recipes;
        """,
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE comments (
            id SERIAL PRIMARY KEY NOT NULL,
            content TEXT NOT NULL,
            recipe_id INTEGER REFERENCES recipes(id) ON DELETE CASCADE,
            user_id INTEGER NOT NULL REFERENCES users(id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE comments;
        """,
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE likes (
            id SERIAL NOT NULL,
            user_id INTEGER NOT NULL REFERENCES users(id),
            recipe_id INTEGER NOT NULL REFERENCES recipes(id) ON DELETE CASCADE,
            PRIMARY KEY (user_id, recipe_id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE likes;
        """,
    ],
]
