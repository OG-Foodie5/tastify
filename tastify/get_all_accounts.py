from fastapi.testclient import TestClient
from unittest.mock import patch

from main import app
from queries.users import UserOut, UserRepository

client = TestClient(app)

def test_get_all_users():
    expected_users = [UserOut(id=1, username='test1', hashed_password='hashed_password', email='test1@example.com')]
    
    with patch.object(UserRepository, 'get_all', return_value=expected_users):
        response = client.get("/users/all")
        assert response.status_code == 200
        assert response.json() == expected_users, f"Expected {expected_users}, but got {response.json()}"
