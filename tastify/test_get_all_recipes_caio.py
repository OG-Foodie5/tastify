from fastapi.testclient import TestClient
from main import app
from queries.recipes import RecipeRepository

client = TestClient(app)

mock_recipes = {
    "id": 1,
    "title": "str",
    "description": "str",
    "steps": "str",
    "ingredients": "str",
    "picture_url": "str",
    "user_id": 1,
}


class MockRecipes:
    def get_all_recipes(self):
        return [mock_recipes]


def test_get_all_recipes_returns_200():
    app.dependency_overrides[RecipeRepository] = MockRecipes
    response = client.get("/recipes/")
    assert response.status_code == 200
    app.dependency_overrides = {}
