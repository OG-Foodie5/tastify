from fastapi.testclient import TestClient
from main import app
from queries.comments import CommentRepository

client = TestClient(app)

comment_detail_out = {
    "id": 1,
    "content": "This is a test!",
    "recipe_id": 1,
    "user_id": 1,
}


class MockCommentQueries:
    def get_one_comment(self, comment_id):
        return comment_detail_out


def test_get_comment_detail():
    request = {
        "id": 1,
        "content": "This is a test!",
        "recipe_id": 1,
        "user_id": 1,
    }
    app.dependency_overrides[CommentRepository] = MockCommentQueries
    response = client.get("/comments/1", json=request)
    actual = response.json()
    assert actual == comment_detail_out
    assert response.status_code == 200
