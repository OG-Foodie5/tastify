from fastapi.testclient import TestClient
from main import app
from auth import authenticator
from queries.recipes import RecipeRepository

client = TestClient(app)


class CreateRecipeQueries:
    def create(self, recipe):
        result = {
            "id": 1,
            "title": "string",
            "description": "string",
            "steps": "string",
            "ingredients": "string",
            "picture_url": "string",
            "user_id": 1,
        }
        result.update(recipe)
        return result


mock_data = {
    "title": "string",
    "description": "string",
    "steps": "string",
    "ingredients": "string",
    "picture_url": "string",
    "user_id": 0,
}


def account_override():
    return mock_data


def test_create_recipe():
    app.dependency_overrides[RecipeRepository] = CreateRecipeQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = account_override
    json = {
        "title": "string",
        "description": "string",
        "steps": "string",
        "ingredients": "string",
        "picture_url": "string",
        "user_id": 10,
    }

    expected = {
        "id": 1,
        "title": "string",
        "description": "string",
        "steps": "string",
        "ingredients": "string",
        "picture_url": "string",
        "user_id": 10,
    }
    response = client.post("/recipes", json=json)

    app.dependency_overrides = {}
    assert response.status_code == 200
    assert response.json() == expected


def test_init():
    assert 1 == 1
