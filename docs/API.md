## Recipes 
| Action | Method | Path  | Request Body  | Returns
|-----------------------|:-------------|:---------------:|---------------:|---------------:|
| Get All Recipes | GET  | http://localhost:8000/recipes | | ```{"recipes": [{"title": "food", "description": "string", "ingredients": "rice","steps": "string", "picture_url": "string", "id": 2, "user": {"username": "lauren", "id": 1}}]}``` |
| Create a Recipe | POST | http://localhost:8000/recipes | ```{"title": "food", "description": "string", "steps": "string", "ingredients": "rice", "picture_url": "string", "user_id": 1}``` | ```{"id": 2, "title": "food","description": "string", "steps": "string", "ingredients": "rice", "picture_url": "string", "user_id": 1}``` |
| Get One Recipe | GET | http://localhost:8000/recipes/2 | | ```{"id": 2, "title": "food", "description": "string","steps": "string", "ingredients": "rice", "picture_url": "string", "user_id": 1}``` |
| Update a Recipe | PUT | http://localhost:8000/recipes/2 | ```{"title": "pizza", "description": "string", "steps": "string", "ingredients": "string", "picture_url": "string", "user_id": 1}``` | ```{"id": 2, "title": "pizza","description": "string", "steps": "string", "ingredients": "string", "picture_url": "string", "user_id": 1}}```
| Delete a Recipe | DELETE | http://localhost:8000/recipes/2 | | true |

## Likes 
| Action | Method | Path  | Request Body  | Returns
|-----------------------|:-------------|:---------------:|---------------:|---------------:|
| Get Likes | GET  | http://localhost:8000/likes | | ```{"likes": [{"id": 2, "recipe_id": 3, "user_id": 1}]}``` |
| Create a Like | POST | http://localhost:8000/like | ```{"recipe_id": 3, "user_id": 1}``` | ```{"id": 2,"recipe_id": 3, "user_id": 1}``` |
| Get a Like | GET | http://localhost:8000/likes/2 | | ```{"id": 2, "recipe_id": 3, "user_id": 1}``` |
| Remove a Like | DELETE | http://localhost:8000/delete/like?like_id=2 | | true |

## Users 
| Action | Method | Path  | Request Body  | Returns
|-----------------------|:-------------|:---------------:|---------------:|---------------:|
| Get All Users | GET  | http://localhost:8000/users/all | | ```[{"id": 1, "username": "lauren", "hashed_password": "$2b$12$4buwsFU6SCoK3fqMNJsPYOGdLsX3HLRRQVtCMcQKhDnnDyUBfmd6K", "email": "string@test.com"}]``` |
| Create Account | POST | http://localhost:8000/signup | ```{"username": "lolo", "password": "string", "email": "lolo@test.com"}``` | ```{"access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIzZWJkMDdmNi1hN2Y4LTQ1ODctYjA3Yy05ZWIwMjk4ODU1MjciLCJleHAiOjE2Nzg0MjcwNzgsInN1YiI6ImxvbG8iLCJhY2NvdW50Ijp7ImlkIjoyLCJ1c2VybmFtZSI6ImxvbG8iLCJoYXNoZWRfcGFzc3dvcmQiOiIkMmIkMTIkNVBDbnF3UWlRbGVMVUFBZGNSSjcwZTZoS21naDVsZm9Ta3R4eFdIL1VsLm5XbHJicEpjWVMiLCJlbWFpbCI6ImxvbG9AdGVzdC5jb20ifX0.OrPNyLYJzgUiKu7AVRYZSusl_y3Gs-hF_gNd3MdqHHM","token_type": "Bearer", "account": {"id": 2, "username": "lolo", "hashed_password": "$2b$12$5PCnqwQiQleLUAAdcRJ70e6hKmgh5lfoSktxxWH/Ul.nWlrbpJcYS", "email": "lolo@test.com"}}``` |
| Get User | GET | http://localhost:8000/get/lauren | | ```{"id": 1, "username": "lauren", "hashed_password": "$2b$12$4buwsFU6SCoK3fqMNJsPYOGdLsX3HLRRQVtCMcQKhDnnDyUBfmd6K", "email": "string@test.com"}``` |

## Comments 
| Action | Method | Path  | Request Body  | Returns
|-----------------------|:-------------|:---------------:|---------------:|---------------:|
| Get All Comments | GET  | http://localhost:8000/recipes | | ```{"recipes": [{"title": "food", "description": "string", "ingredients": "rice","steps": "string", "picture_url": "string", "id": 2, "user": {"username": "lauren", "id": 1}}]}``` |
| Create a Comment | POST | http://localhost:8000/recipes | ```{"title": "food", "description": "string", "steps": "string", "ingredients": "rice", "picture_url": "string", "user_id": 1}``` | ```{"id": 2, "title": "food","description": "string", "steps": "string", "ingredients": "rice", "picture_url": "string", "user_id": 1}``` |
<!-- | Get a Comment | GET | http://localhost:8000/create
 | | ```{}``` |
| Delete a Comment | DELETE | http://localhost:8000/comments/1 | | true | -->