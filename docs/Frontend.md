# Wireframe

## Create an Account
![Created Account](./images/create_account.png)

## Create a Recipe
![Create Recipe](./images/create_recipe.png)

## Landing Page
![Landing Page](./images/landing_page.png)

## Recipe Detail
![Recipe Detail](.images/recipe_detail.png)

## Sign In
![Sign In](./images/sign_in.png)