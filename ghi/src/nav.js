import { NavLink } from "react-router-dom";
import { useToken } from "./Auth";

const Nav = () => {
  const [token, , logout] = useToken();

  return (
    <nav className="navbar navbar-expand-xxxl navbar-light bg-info">
      <div className="container-fluid">
          <a href="https://og-foodie5.gitlab.io/tastify/" className="navbar-brand">Tastify</a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mea-auto mb-2 mb-lg-0">
            {!token && (
              <>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/login">
                    Login
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/signup">
                    Sign up
                  </NavLink>
                </li>
              </>
            )}
            {token && (
              <>
                <li className="nav-item">
                  <span
                    onClick={() => {
                      logout();
                    }}
                    role={"button"}
                    className="nav-link"
                  >
                    Logout
                  </span>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/recipes/new">
                    Create recipe
                  </NavLink>
                </li>
              </>
            )}
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Nav;
