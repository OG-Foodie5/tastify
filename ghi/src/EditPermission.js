function EditPermission() {
    return (
        <div className="App">
            <header className="App-header">
                <h1 className="title">You don't have permission to edit this recipe</h1>
                <a href="/tastify" className="btn btn-info" role="button"  id="recipeButton">Back to home page</a>

            </header>
        </div>
    )
}

export default EditPermission;
