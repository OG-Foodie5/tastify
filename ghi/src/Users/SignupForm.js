import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function SignupForm() {
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const navigate = useNavigate();

    // const handleUsernameChange = event => {
    //     setUsername(event.target.value)
    // }

    const handleSubmit = async (event) => {
        event.preventDefault();

    const data = {};

    data.username = username;
    data.password = password;
    data.email = email;

    const locationUrl = `${process.env.REACT_APP_TASTIFY_HOST}signup`;
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        "Content-Type": "application/json",
        },
    };

    try {
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
        navigate("/login");
        }
    } catch (error) {
        navigate("/login");
    }
    };


    return (
    <>
        <div className="vh-100" style={{ display: 'flex', flexDirection: "column", alignItems: 'center', justifyContent: 'space-between',backgroundColor: "#02BBD7" }} >
            <form onSubmit={handleSubmit} className="bg-light rounded p-5" style={{ display: 'flex', flexDirection: "column", alignItems: 'center', justifyContent: 'space-between',backgroundColor: "white", marginTop: '300px' }} >
            <h3 id="signup-title">
                <strong>Sign Up</strong>
            </h3>
            <input
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                type="text"
                placeholder="Username"
                style={{ marginBottom: '15px'}}
            />
            <input
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                type="text"
                placeholder="Email"
                style={{ marginBottom: '15px'}}
            />
            <input
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                type="password"
                placeholder="Password"
                style={{ marginBottom: '15px'}}
            />
            <button className="btn btn-primary signup-btn">Submit</button>
            <div className="login-link">
                <strong>Already have an account? </strong>
                <a href="./login">
                    <u>Login</u>
                </a>
            </div>
            </form>
        </div>
    </>
    );
}
export default SignupForm;
