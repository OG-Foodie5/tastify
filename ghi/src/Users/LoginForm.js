import { useState } from 'react';
import {useNavigate} from 'react-router-dom';
import {useToken } from '../Auth';


function LoginForm(props) {
    const[username, setUsername] = useState('');
    const[password, setPassword] = useState('');
    const[,login] = useToken();
    const navigate = useNavigate();

        const handleSubmit = async (event) => {
            event.preventDefault();
            const error = await login(username, password);
            if (error) {
                alert('Incorrect login username or password')
                setUsername('');
                setPassword('');
                return
                //islogged(false)
            }
            navigate('/');

        }
        return (
          <>
            <div
              className="row justify-content-center align-items-center vh-100"
              style={{ backgroundColor: "#02BBD7" }}
            >
              <div className="col col-sm-6 col-md-6 col-lg-4 col-xl-3">
                <form className="bg-light rounded p-5">
                  <div className="text-center mb-3">
                    <h5>Sign in</h5>
                  </div>
                  <div className="form-group mb-3">
                    <span className="username"></span>
                    <input
                      value={username}
                      type="username"
                      onChange={(event) => setUsername(event.target.value)}
                      className="form-control"
                      id="username"
                      aria-describedby="username"
                      placeholder="Username"
                    />
                  </div>
                  <div className="form-group mb-3">
                    <span className="password"></span>
                    <input
                      value={password}
                      type="password"
                      onChange={(event) => setPassword(event.target.value)}
                      className="form-control"
                      id="password"
                      aria-describedby="password"
                      placeholder="Enter Password"
                    />
                  </div>
                  <button
                    className="btn btn-primary btn-block mt-3 w-100"
                    onClick={handleSubmit}
                    type="Submit"
                  >
                    Login
                  </button>
                  <div className="text-center mt-5">
                    <div className="text-center mt-3">
                      <strong>Don't have an account? </strong>
                      <a href="./signup">
                        <u>Sign up</u>
                      </a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </>
        );
}
export default LoginForm;
