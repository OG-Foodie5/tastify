import { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './nav';
import './App.css';
import './Nav.css'
import './Recipes/Detail.css';
import './index.css'
import LoginForm from './Users/LoginForm';
import { AuthProvider, useToken } from './Auth';
import RecipeDetail from './Recipes/RecipeDetail.js';
import RecipeList from './Recipes/RecipeList';
import SignupForm from './Users/SignupForm';
import NewRecipeForm from './Recipes/NewRecipeForm';
import EditRecipeForm from './Recipes/EditRecipeForm';
import EditPermission from './EditPermission';


function GetToken() {
  useToken();
  return null
}

function App() {
  const [recipes, setRecipes] = useState([]);
  const [comments, setComments] = useState([]);
  const [likes, setLikes] = useState([]);

  const getRecipes = async () => {
    const url = `${process.env.REACT_APP_TASTIFY_HOST}recipes`;
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setRecipes(data.recipes);
    }
  };

  const getComments = async () => {
  const url = `${process.env.REACT_APP_TASTIFY_HOST}comments/all`;
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    const comments = data.comments
    setComments(comments);
  }
  };

  const getLikes = async () => {
  const url = `${process.env.REACT_APP_TASTIFY_HOST}likes`;
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    const likes = data.likes
    setLikes(likes);
  }
  };


  useEffect(() => {
    getRecipes();
    getComments();
    getLikes();
  }, []
  )

  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');


  return (
  <BrowserRouter basename={basename}>
    <AuthProvider>
      <GetToken />
      <Nav />
            <Routes>
              <Route path="/" element={<RecipeList recipes={recipes}/>} />
              <Route path="/signup" element={<SignupForm />} />
              <Route path="/login" element={<LoginForm />} />
              <Route path="/permission" element={<EditPermission />} />
              <Route path="/recipes">
                  <Route path="new" element={<NewRecipeForm/>} />
                  <Route path=':id/edit' element={<EditRecipeForm />} />
              </Route>

              {recipes.map((recipe) => (
                <Route key={recipe.id} id={recipe.id} path="recipes/:id"
                  element={<RecipeDetail recipes={recipes} comments={comments} likes={likes} getRecipes={getRecipes} getComments={getComments}  />
                  }
                />
              ))}
        </Routes>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
