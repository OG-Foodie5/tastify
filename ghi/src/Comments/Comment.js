/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useAuthContext } from '../Auth'

const CommentForm = ({ recipes }) => {
    const [content, setContent] = useState("");
    const [userId, setUserId] = useState("");
    const [, setRecipeId] = useState("");
    const navigate = useNavigate();
    const { token } = useAuthContext();


    const handleContentChange = event => {
        setContent(event.target.value)
    }


    const { id } = useParams();

    const fetchUser = async () => {
        const url = `${process.env.REACT_APP_TASTIFY_HOST}token`;
        const response = await fetch(url, {
            credentials: "include",
        });

        if (response.ok) {
            const data = await response.json();
            const userId = data.account.id;
            setUserId(userId)

        }
    }

        const fetchRecipe = async (recipe) => {
        const recipeURL = `${process.env.REACT_APP_TASTIFY_HOST}recipes/${recipe.id}`

        const response = await fetch(recipeURL, fetchRecipe);
        if (response.ok){
            const recipeId = Number(id)
            setRecipeId(recipeId)
        }
    }


    useEffect(() => {
        fetchUser();
        if (token === false) {
            navigate("/login")
        }
    }, [token, navigate])

    const handleSubmit = async(event) => {
        event.preventDefault();
        const data = {}

        data.content = content
        data.recipe_id = Number(id)
        data.user_id = Number(userId)

        const url = `${process.env.REACT_APP_TASTIFY_HOST}create`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            }
        };

        const response = await fetch(url, fetchConfig)
        if(response.ok) {
            const newComment = await response.json()
            setContent("");
            navigate(`recipes/${recipes.id}`)
        }
    };



    return (
        <div className="container">

        <h3><small>Add a comment!</small></h3>
        <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
                <textarea onChange={handleContentChange} placeholder="Comment" required type="text" name="comment" className="form-control" value={content} />
                <label htmlFor="title">Please be polite</label>
            </div>

            <button className="buttons" id="recipeButton">Add comment</button>
        </form>
    </div>

    )
}


export default CommentForm;
