/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useAuthContext } from '../Auth'
import EditPermission from "../EditPermission";

const EditRecipeForm = () => {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [steps, setSteps] = useState("");
    const [ingredients, setIngredients] = useState("");
    const [pictureURL, setPictureURL] = useState("");
    const [loggedId, setLoggedId] = useState("");
    const [author, setAuthor] = useState("")
    const navigate = useNavigate();
    const [, setRecipe] = useState([]);
    const { token } = useAuthContext();
    const { id } = useParams();


    const handleTitleChange = event => {
        setTitle(event.target.value)
    }

    const handleDescriptionChange = event => {
        setDescription(event.target.value)
    }

    const handleStepsChange = (event) => {
        setSteps(event.target.value)
    }

    const handleIngredientsChange = (event) => {
        setIngredients(event.target.value)
    }

    const handlePictureUrl = event => {
        setPictureURL(event.target.value)
    }

    const fetchUser = async () => {
        const url = `${process.env.REACT_APP_TASTIFY_HOST}token`;
        //replace url with variable so it works w/ deployment
        const response = await fetch(url, {
            credentials: "include",
        });

        if (response.ok) {
            const data = await response.json();
            const loggedId = data.account.id;
            setLoggedId(loggedId)
        }
    }

    const fetchRecipe = async () => {
        const url = `${process.env.REACT_APP_TASTIFY_HOST}recipes/${id}`;
        const response = await fetch(url, {
            credentials: "include",
        });

        if (response.ok) {
            const data = await response.json();
            setTitle(data.title)
            setDescription(data.description)
            setSteps(data.steps)
            setIngredients(data.ingredients)
            setPictureURL(data.picture_url)
            setAuthor(data.user_id)
            setRecipe(data)
        }
    }

    useEffect(() => {
        fetchRecipe();
    }, [])

        useEffect(() => {
        fetchUser();
        // if (author !== loggedId) {
        //     navigate("/permission")
        // }
    }, [navigate, loggedId, author])


    const handleSubmit = async(event) => {
        event.preventDefault();
        const data = {}

        data.title = title
        data.description = description
        data.steps = steps
        data.ingredients = ingredients
        data.picture_url = pictureURL
        data.user_id = author

        const url = `${process.env.REACT_APP_TASTIFY_HOST}recipes/${id}`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            }
        };

        const response = await fetch(url, fetchConfig)
        if(response.ok) {
            const newRecipe = await response.json()
            setTitle("");
            setDescription("");
            setSteps("");
            setIngredients("");
            setPictureURL("");
            navigate(`/recipes/${newRecipe.id}/`)
        }
    };

    return (loggedId === author ?
        <div className="container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 className="text-center">Edit Recipe</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" className="form-control" value={title} />
                            <label htmlFor="title">Title:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea onChange={handleDescriptionChange} id="textBox" placeholder="Description" rows="4" name="description" className="form-control" value={description}></textarea>
                            <label htmlFor="description">Description:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea onChange={handleStepsChange} id="textBox" placeholder="Steps" rows={1} cols={40} name="steps" className="form-control" value={steps}></textarea>
                            <label htmlFor="steps">Steps:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea onChange={handleIngredientsChange} id="textBox" placeholder="Ingredients" required type="text" name="ingredients" className="form-control" value={ingredients}></textarea>
                            <label htmlFor="ingredients">Ingredients:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrl} placeholder="URL" required type="url" name="pictureURL" className="form-control" value={pictureURL} />
                            <label htmlFor="url">URL:</label>
                        </div>
                        <button className="btn btn-primary mr-1" id="recipeButton">Edit Recipe</button>
                        <a href="/tastify/recipes" className="btn btn-info" role="button"  id="cancelButton">Cancel Edit</a>
                    </form>
                </div>
            </div>
        </div>
        : <EditPermission />)
}






export default EditRecipeForm;
