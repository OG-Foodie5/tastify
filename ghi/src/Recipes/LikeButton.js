import { useEffect, useState } from 'react';
import { useParams } from "react-router-dom";

const LikeButton = ({ likes, getLikes }) => {

  const { id } = useParams();
  const recipeId = Number(id);

  const [isClicked, setIsClicked] = useState(false);

  // Find the like for the current recipe
  const [currentLike, setCurrentLike] = useState(null);
  useEffect(() => {
    setCurrentLike(likes.find((like) => like.recipe_id === recipeId));
  }, [likes, recipeId]);

  const [liked, setLiked] = useState(0);
  useEffect(() => {
    setLiked(currentLike ? 1 : 0);
  }, [currentLike]);

  const addLike = async () => {
    const likeURL = `${process.env.REACT_APP_TASTIFY_HOST}likes/${currentLike.id}`;
    const fetchConfig = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ recipe_id: recipeId }),
    };

    const response = await fetch(likeURL, fetchConfig);
    if (response.ok) {
      getLikes();
    }
  };

  const deleteLike = async () => {
    const likeURL = `${process.env.REACT_APP_TASTIFY_HOST}likes/${currentLike.id}`;
    const fetchConfig = {
      method: 'DELETE',
    };

    const response = await fetch(likeURL, fetchConfig);
    if (response.ok) {
      getLikes();
    }
  };

  const handleClick = () => {
    if (isClicked) {
      setLiked(liked - 1);
      deleteLike();
    } else {
      setLiked(liked + 1);
      addLike();
    }
    setIsClicked(!isClicked);
  };

  const likeCount = likes.filter((like) => like.recipe_id === recipeId).length;

  return (
    <div>
      <button
        id="likes"
        className={`like-button ${isClicked && 'liked'}`}
        onClick={handleClick}
      >
        <span className="like-counter">{` ♥ likes: ${likeCount + liked} `}</span>
      </button>
    </div>
  );
};

export default LikeButton;
