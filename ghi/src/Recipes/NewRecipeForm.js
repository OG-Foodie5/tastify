import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useAuthContext } from '../Auth'

const NewRecipeForm = () => {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [steps, setSteps] = useState("");
    const [ingredients, setIngredients] = useState("");
    const [pictureURL, setPictureURL] = useState("");
    const [userId, setUserId] = useState("");
    const navigate = useNavigate();
    const { token } = useAuthContext();


    const handleTitleChange = event => {
        setTitle(event.target.value)
    }

    const handleDescriptionChange = event => {
        setDescription(event.target.value)
    }

    const handleStepsChange = (event) => {
        setSteps(event.target.value)
    }

    const handleIngredientsChange = (event) => {
        setIngredients(event.target.value)
    }

    const handlePictureUrl = event => {
        setPictureURL(event.target.value)
    }

    const fetchUser = async () => {
        const url = `${process.env.REACT_APP_TASTIFY_HOST}token`;
        const response = await fetch(url, {
            credentials: "include",
        });

        if (response.ok) {
            const data = await response.json();
            const userId = data.account.id;
            setUserId(userId)

        }
    }

    useEffect(() => {
        fetchUser();
        if (token === false) {
            navigate("/login")
        }
    }, [token, navigate])

    const handleSubmit = async(event) => {
        event.preventDefault();
        const data = {}

        data.title = title
        data.description = description
        data.steps = steps
        data.ingredients = ingredients
        data.picture_url = pictureURL
        data.user_id = userId

        const url = "https://tastify.nov-pt-9.mod3projects.com/recipes"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            }
        };

        const response = await fetch(url, fetchConfig)
        if(response.ok) {
            const newRecipe = await response.json()
            setTitle("");
            setDescription("");
            setSteps("");
            setIngredients("");
            setPictureURL("");
            navigate(`/recipes/${newRecipe.id}`)
        }
    };

    return (
        <div className="container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 className="text-center">Create a Recipe</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" className="form-control" value={title} />
                            <label htmlFor="title">Title:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea onChange={handleDescriptionChange} id="textBox" placeholder="Description" rows="4" name="description" className="form-control" value={description}></textarea>
                            <label htmlFor="description">Description:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea onChange={handleStepsChange} id="textBox" placeholder="Steps" rows={1} cols={40} name="steps" className="form-control" value={steps}></textarea>
                            <label htmlFor="steps">Steps:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea onChange={handleIngredientsChange} id="textBox" placeholder="Ingredients" required type="text" name="ingredients" className="form-control" value={ingredients}></textarea>
                            <label htmlFor="ingredients">Ingredients:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrl} placeholder="URL" required type="url" name="pictureURL" className="form-control" value={pictureURL} />
                            <label htmlFor="url">URL:</label>
                        </div>
                        <button className="btn btn-primary text-right" id="recipeButton">Create Recipe</button>
                    </form>
                </div>
            </div>
        </div>
    )
}





export default NewRecipeForm;
