/* eslint-disable jsx-a11y/anchor-is-valid */
import { useNavigate, useParams } from "react-router-dom";
import React, { useEffect } from "react";
import CommentForm from "../Comments/Comment";
import LikeButton from "./LikeButton";


function RecipeDetail({ recipes, getRecipes, comments, getComments, likes, getLikes, recipelikes}){

    const { id } = useParams();
    const IDDD = Number(id)

const specific_recipes = recipes.filter(recipe => recipe.id === IDDD)
const specific_comments = comments.filter(comment => comment.recipe_id === IDDD)




    const deleteRecipe = async (recipe) => {
        const recipeURL = `${process.env.REACT_APP_TASTIFY_HOST}recipes/${recipe.id}`
        const fetchConfig = {
            method: 'delete'
        };

        const response = await fetch(recipeURL, fetchConfig);
        if (response.ok){
            getRecipes();
        }
    }

    // sends you back to home page (after delete)
    const nav = useNavigate("/home");


    useEffect(() => {
        getRecipes();
    }, [getRecipes])

        useEffect(() => {
        getComments();
    }, [getComments])


    return(
        <>
        <div className="detail">
            <div>


            {specific_recipes && specific_recipes.map((recipe) => {
                return (
                        <>
                        <h1>{recipe.title}</h1>
                        <div className="underline"></div>
                        <img className="image" src={ recipe.picture_url } alt="food" width="500" height="333"/>

                <LikeButton likes={likes} />

                <h2>About</h2>
                <p className="card-text text-muted">{recipe.description}</p>

                <h2>Ingredients</h2>
                <p>{recipe.ingredients}</p>

                <h3>Steps</h3>
                <p>{recipe.steps}</p>

                <a href={`${recipe.id}/edit/`} className="buttons">✔ Edit recipe</a>
                <a className="delete-button" title="farewell!" id={recipe.id} onClick={() => {deleteRecipe(recipe); nav("/")}}>🞭 Delete Recipe</a>
                <p key={recipe.id}>
                    <p className="card-text text-muted"><i>Written by: {recipe.user.username}</i></p>
                </p>

            <div className="line"></div>
                    <h2>Comments</h2>
                    {specific_comments && specific_comments.map((comment) => {
                            return (
                                <tr key={comment.id}>

                                    <p>❛❛ {comment.content} ❜❜</p>
                                </tr>
                                );
                            })}
                        <CommentForm recipes={recipes.id}/>
                </>
                    );
                })}

                </div>
        </div>

        </>
    );
}
export default RecipeDetail;
