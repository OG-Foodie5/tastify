function RecipeList({ recipes }){
    return(
        <>

        <div className="mainpage">
            <div className="recipe-container-cols">
            {recipes && recipes.map((recipe) => {
                return (
                    <>
                    <div key={recipe.href} className="card mb3 shadow rec-cards">
                        <img className="card-img-top" src={ recipe.picture_url } alt="food"
                        style={{
                            width: '100%',
                            height: '100%',
                            objectFit: 'fixed',
                        }}
                        />
                        <div className="card-body" >
                            <h1><a href={`recipes/${recipe.id}`} className="recipetitlelink">{recipe.title}</a></h1>
                            <p className="card-text text-muted">{recipe.description}</p>
                        </div>
                        <div className="creator">🧑🏻‍🍳   {recipe.user.username}
                        </div>
                    </div>
                </>

                    );
                })}

                </div>
        </div>

        </>
    );
}
export default RecipeList;
