# Tastify

- Tastify is a web application for foodies who are interested in either creating their own or viewing other foodies' recipes.

- Access the live app at https://og-foodie5.gitlab.io/tastify/

## Features

- Users can sign-up, login, and logout.
- After logging in, users can create, view, update, and delete their recipes.
- After logging in, users can view view, like, and comment on recipes.

## Design

- [API](docs/API.md)
- [Wireframe](docs/Frontend.md)

## Initialization

- Fork repo
- Clone repo `git clone https://gitlab.com/OG-Foodie5/tastify`
- `cd` into new project directory
- Run `docker volume create pg-admin`
- Run `docker volume create postgres-data`
- Run `docker compose build`
- Run `docker compose up`
- And...

## Enjoy

- Access the app from the frontend at http://localhost:3000/
- Access the app from the backend at http://localhost:8000/docs

## Team

- Caio Lima
- Hana Machrus
- Jake Thompson
- Jose Ayala
- Lauren Alpuerto
